import control as clt
import matplotlib.pyplot as plt
import csv
from scipy.signal import lti, step


#Parâmetros do controlador
Kp = 10.0
Ki = 10.0
Kd = 1.0

print("Kp= ",Kp)
print("Ki= ",Ki)
print("Kd= ",Kd)

#Pontos de resolução
dots = 5000


#Importa dados do CSV do matlab
tM=[]
yM=[]

filename = 'database\data[Kp='+str(Kp)+' Ki='+str(Ki)+' Kd='+str(Kd)+'].csv'
with open(filename, 'rt') as f:
	reader = csv.reader(f)
	for row in reader:
		tM.append(float(row[0]))
		yM.append(float(row[1]))
		
#Importa dados do CSV do matlab
tMa=[]
yMa=[]

filename = 'database\dataMat[Kp='+str(Kp)+' Ki='+str(Ki)+' Kd='+str(Kd)+'].csv'
with open(filename, 'rt') as f:
	reader = csv.reader(f)
	for row in reader:
		tMa.append(float(row[0]))
		yMa.append(float(row[1]))
	
#sistema
k = 1
wn = [1,5]
a = 1
b = sum(wn)
c = wn[0]*wn[1]

num = [1]
den = [a,b,c]
#Monta a FT
G = clt.TransferFunction([k],[a, b, c])
print("G(s)= ",G)


#Monta o sistema de controle
controler = Kp*(1+clt.TransferFunction([Ki],[1,0])+clt.TransferFunction([Kd,0],[1]))
print("controler= ",controler)

#Realimenta o sistema com o controlador aplicado
print("controler*G= ",controler*G)
sys = clt.feedback(controler*G)
print("sys= ",sys)
#Aplica o degrau
sys2 = sys.returnScipySignalLTI()[0][0]
t2,y2 = step(sys2,N = dots)
t,y   = clt.step_response(sys)

#Faz um degrau só para exibir gráficamente
Tstep = [-0.5,0,1e-15,t.max()]
Ystep = [0,0,1,1]


#prepara algumas informações da legenda
Pico = max(y)
print("Pico = ",Pico)
Final = 1
print("Estabiliza em  = ",y[-1])
SobreSinal_c = 100*(Pico-Final)/Final
print("SobreSinal_c = ",SobreSinal_c,"%")
Erro = (1-y[-1])*100
print("Erro = ",Erro,"%")


#Ajusta o tempo máximo do Simulink para ficar do mesmo tamanho do Python
for x in range(len(tM)):
	if(tM[x]>t[-1]): break
tM = tM[:x]
yM = yM[:x]

#prepara algumas informações da legenda
PicoM = max(yM)
FinalM = 1
SobreSinal_cM = 100*(PicoM-FinalM)/FinalM
ErroM = (1-yM[-1])*100

#Ajusta o tempo máximo do Matblab para ficar do mesmo tamanho do Python
for x in range(len(tMa)):
	if(tMa[x]>t[-1]): break
tMa = tMa[:x]
yMa = yMa[:x]

#prepara algumas informações da legenda
PicoMa = max(yMa)
FinalMa = 1
SobreSinal_cMa = 100*(PicoMa-FinalMa)/FinalMa
ErroMa = (1-yMa[-1])*100

#Ajusta o tempo máximo do scipy para ficar do mesmo tamanho do control
for x in range(len(t2)):
	if(t2[x]>t[-1]): break
t2 = t2[:x]
y2 = y2[:x]

#prepara algumas informações da legenda
Pico2 = max(y2)
Final2 = 1
SobreSinal_c2 = 100*(Pico2-Final2)/Final2
Erro2 = (1-y2[-1])*100


#Monta o Gráfico
plt.title('Step Response - (Kp='+str(Kp)+' Ki='+str(Ki)+' Kd='+str(Kd)+')')                  # Titulo
plt.plot(t,y,label='control lib; MP = '+str(round(SobreSinal_c,3))+'%' )
plt.plot(t2,y2,label='scipy lib; MP = '+str(round(SobreSinal_c2,3))+'% with '+str(dots)+' dots')
plt.plot(tM,yM,label='Simulink; MP = '+str(round(SobreSinal_cM,3))+'%' )
plt.plot(tMa,yMa,label='Matlab; MP = '+str(round(SobreSinal_cMa,3))+'%' )
plt.plot(Tstep,Ystep,label='Step')
plt.ylabel('gain')        # Plota o label y
plt.xlabel('time [s]')            # Plota o label x
plt.grid(which='both', axis='both')     # Gride para frequências intermediárias
plt.grid(True)                          # Mostra o Grid
plt.margins(0, 0.1)                     # Deixa uma margem
plt.legend()
plt.savefig('Fig\degrau-[Kp='+str(Kp)+' Ki='+str(Ki)+' Kd='+str(Kd)+'].png')




#Ajusta o tempo máximo para inspecionar
zoom = 2

for x in range(len(t)):
	if(t[x]>zoom): break
t = t[:x+2]
y = y[:x+2]

for x in range(len(tM)):
	if(tM[x]>t[-1]): break
tM = tM[:x+1]
yM = yM[:x+1]

for x in range(len(tMa)):
	if(tMa[x]>t[-1]): break
tMa = tMa[:x+1]
yMa = yMa[:x+1]

for x in range(len(t2)):
	if(t2[x]>t[-1]): break
t2 = t2[:x+1]
y2 = y2[:x+1]




#Monta o Gráfico
plt.figure()
plt.title('Zoom on error - (Kp='+str(Kp)+' Ki='+str(Ki)+' Kd='+str(Kd)+')')                  # Titulo
plt.plot(t,y,label='control lib')#,marker=".")
plt.plot(t2,y2,label='scipy lib with '+str(dots)+' dots')#,marker=".")
plt.plot(tM,yM,label='Simulink')#, marker=".")
plt.plot(tMa,yMa,label='Matlab')#, marker=".")
plt.ylabel('gain')        # Plota o label y
plt.xlabel('time [s]')            # Plota o label x
plt.grid(which='both', axis='both')     # Gride para frequências intermediárias
plt.grid(True)                          # Mostra o Grid
plt.margins(0, 0.1)                     # Deixa uma margem
plt.legend()
plt.savefig('Fig\ZoomOnError-[Kp='+str(Kp)+' Ki='+str(Ki)+' Kd='+str(Kd)+'].png')

plt.show()