import control as clt
import matplotlib.pyplot as plt
import csv
from scipy.signal import lti, step

#Pontos de resolução
dots = 5000

#Ajusta o tempo máximo para inspecionar
zoom = 4
jump1 = 1000
jump2 = 100000

#Parâmetros do controlador
aSolver = ["ode1","ode2","ode3","ode4","ode5","ode8","ode14x"];
#arrayKp = [1.0,  1.0,  1.0, 10.0, 10.0];
#arrayKi = [0.1,  0.1, 10.0,  0.1, 10.0];
#arrayKd = [1.0, 10.0,  1.0,  1.0, 1.0 ];

arrayKp = [1.0,  1.0, 1.0, 10.0, 10.0, 10.0];
arrayKd = [0.1, 10.0, 1.0,  0.1, 10.0,  1.0];
arrayKi = [0.0,  0.0, 0.0,  0.0,  0.0,  0.0];


for nP in range(len(arrayKp)):
	Kp = arrayKp[nP]
	Ki = arrayKi[nP]
	Kd = arrayKd[nP]
	
	#sistema
	k = 1
	wn = [1,5]
	a = 1
	b = sum(wn)
	c = wn[0]*wn[1]

	num = [1]
	den = [a,b,c]
	#Monta a FT
	G = clt.TransferFunction([k],[a, b, c])
	print("G(s)= ",G)


	#Monta o sistema de controle
	controler = Kp*(1+clt.TransferFunction([Ki],[1,0])+clt.TransferFunction([Kd,0],[1]))
	print("controler= ",controler)

	#Realimenta o sistema com o controlador aplicado
	print("controler*G= ",controler*G)
	sys = clt.feedback(controler*G)
	print("sys= ",sys)
	#Aplica o degrau
	sys2 = sys.returnScipySignalLTI()[0][0]
	t,y = step(sys2,N = dots)

	#prepara algumas informações da legenda
	Pico = max(y)
	print("Pico = ",Pico)
	Final = 1
	print("Estabiliza em  = ",y[-1])
	SobreSinal_c = 100*(Pico-Final)/Final
	print("SobreSinal_c = ",SobreSinal_c,"%")
	Erro = (1-y[-1])*100
	print("Erro = ",Erro,"%")
	
	#Faz um degrau só para exibir gráficamente
	Tstep = [-0.5,0,1e-15,t.max()]
	Ystep = [0,0,1,1]

	#Monta o Gráfico
	fig, ax = plt.subplots(2, 1)
	ax[0].plot(Tstep,Ystep,label='Step')
	ax[0].plot(t,y,label='scipy lib')#; MP = '+str(round(SobreSinal_c,3))+'% with '+str(dots)+' dots')
	
	#Faz um degrau só para exibir gráficamente
	Tstep = [-0.05,0,1e-15,zoom]
	Ystep = [0,0,1,1]
	ax[1].plot(Tstep,Ystep,label='Step')#,marker=".")
	
	for x in range(len(t)):
		if(t[x]>zoom): break
	tc = t[:x+1]
	yc = y[:x+1]
	ax[1].plot(tc,yc,label='scipy lib')#,marker=".")
	
	for nS in range(1):#len(aSolver)):
		#Importa dados do CSV do matlab
		solver = aSolver[nS]
		tM=[]
		yM=[]
		line = 0
		filename = 'database\data_'+solver+'[Kp='+str(Kp)+' Ki='+str(Ki)+' Kd='+str(Kd)+'].csv'
		with open(filename, 'rt') as f:
			reader = csv.reader(f)
			for row in reader:
				if(not(line%jump1)):
					tM.append(float(row[0]))
					yM.append(float(row[1]))
				if(tM[-1]>t[-1]): break
				#if(tM[-1]>zoom):jump1 = jump2
				line += 1
				
		ax[0].plot(tM,yM,label=solver)#, marker=".")
		for x in range(len(tM)):
			if(tM[x]>zoom): break
		tM = tM[:x+1]
		yM = yM[:x+1]
		ax[1].plot(tM,yM,label=solver)
	
	ax[0].set_title('Step Response - SciPy vs Simulink Solvers(Kp='+str(Kp)+' Ki='+str(Ki)+' Kd='+str(Kd)+')')
	ax[0].set_ylabel('gain')        # Plota o label y
	ax[1].set_ylabel('gain')        # Plota o label y
	ax[1].set_xlabel('time [s]')            # Plota o label x
	ax[0].legend()
	ax[0].grid(True)                          # Mostra o Grid
	ax[1].legend()
	ax[1].grid(True)                          # Mostra o Grid
	plt.savefig('FigSolver\degrau-Solvers[Kp='+str(Kp)+' Ki='+str(Ki)+' Kd='+str(Kd)+'].png')

plt.show()