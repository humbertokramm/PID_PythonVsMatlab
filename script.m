%Export data to CSV
solver = ["ode1","ode2"];%,"ode3","ode4","ode5","ode8","ode14x"];
%arrayKp = [1   1   1  10  10];
%arrayKi = [0.1 0.1 10 0.1 10];
%arrayKd = [1   10  1  1   1 ];

arrayKp = [1   1  1  10  10 10];
arrayKd = [0.1 10 1  0.1 10 1 ];
arrayKi = [0   0  0  0   0  0 ];


[m,x]= size(solver);
[m,y]= size(arrayKp);
for nS = 1:x
    for nP = 1:y
        Kp = arrayKp(nP)
        Ki = arrayKi(nP)
        Kd = arrayKd(nP)
        ex_pid
        set_param('ex_pid','Solver',solver(nS),'StopTime','400')

        sim('ex_pid');

        formatSpec = 'database\\data_%s[Kp=%.1f Ki=%.1f Kd=%.1f].csv';
        filename = sprintf(formatSpec,solver(nS),Kp,Ki,Kd)
        csvwrite(filename,[ScopeData.time ScopeData.signals.values(:,2)])
    end
end

%clc
%g = zpk([],[-5 -1],1)

%gKi = zpk([],[0],Ki)
%gKd = zpk([0],[],Kd)
%sys = Kp*(1+gKi+gKd)*g
%cLoop = feedback(sys,1)
%figure
%[y,t] = step(cLoop);
%step(cLoop);
%grid on

%formatSpec = 'dataMat[Kp=%.1f Ki=%.1f Kd=%.1f].csv';
%filename = sprintf(formatSpec,Kp,Ki,Kd)
%csvwrite(filename,[t,y])


