# PID Python Vs Matlab
Comparison of the Python "control" and "scipy" library with the Matlab and Simulink on a PID controller.

This repository is intended to compare the response of a simulated PID controller in matlab vs python.

The system created in the Simulink of matlab can be seen in the figure below.
![Screenshot](Fig/Capturar.PNG)


The system response can be seen in the figure below, considering the following parameters:

![Screenshot](Fig/degrau-[Kp%3D1.0%20Ki%3D0.1%20Kd%3D1.0].png)

![Screenshot](Fig/ZoomOnError-[Kp%3D1.0%20Ki%3D0.1%20Kd%3D1.0].png)

![Screenshot](Fig/degrau-[Kp%3D1.0%20Ki%3D0.1%20Kd%3D10.0].png)

![Screenshot](Fig/ZoomOnError-[Kp%3D1.0%20Ki%3D0.1%20Kd%3D10.0].png)

![Screenshot](Fig/degrau-[Kp%3D1.0%20Ki%3D10.0%20Kd%3D1.0].png)

![Screenshot](Fig/ZoomOnError-[Kp%3D1.0%20Ki%3D10.0%20Kd%3D1.0].png)

![Screenshot](Fig/degrau-[Kp%3D10.0%20Ki%3D0.1%20Kd%3D1.0].png)

![Screenshot](Fig/ZoomOnError-[Kp%3D10.0%20Ki%3D0.1%20Kd%3D1.0].png)

![Screenshot](Fig/degrau-[Kp%3D10.0%20Ki%3D10.0%20Kd%3D1.0].png)

![Screenshot](Fig/ZoomOnError-[Kp%3D10.0%20Ki%3D10.0%20Kd%3D1.0].png)

After these tests, it is concluded that the results obtained with the control library, differs from scipy only in terms of resolution, since it is not possible to set a higher sampling rate in the control library.
With regard to matlab, it is noted that its result is close to the result of python.
Actually, the only result that diverges is that of simulink.

Following the tests, a more critical look at the simulink was put.
For comparison only the result of the scipy library was used.
Simulations were generated on all simulink "solvers" for this verification. And they are:
### "ode1";
### "ode2";
### "ode3";
### "ode4";
### "ode5";
### "ode8";
### "ode14x";
All of these "solvers" returned the same result and are available in this repository in .csv format, however the graphics presented only the first and the last of these.


![Screenshot](FigSolver/degrau-Solvers[Kp%3D1.0%20Ki%3D0.1%20Kd%3D1.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D1.0%20Ki%3D0.1%20Kd%3D10.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D1.0%20Ki%3D10.0%20Kd%3D1.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D10.0%20Ki%3D0.1%20Kd%3D1.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D10.0%20Ki%3D10.0%20Kd%3D1.0].png)

Unfortunately, not all data could be made available directly due to the size of the files. Then they were made available within a compressed folder.

To isolate the problem, the next step was to zero the Kd gain, as there was a mistrust that the simulink derivative module could be the source of the problem. Then transforming the system into a PI controller.

![Screenshot](FigSolver/degrau-Solvers[Kp%3D1.0%20Ki%3D0.1%20Kd%3D0.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D1.0%20Ki%3D1.0%20Kd%3D0.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D1.0%20Ki%3D10.0%20Kd%3D0.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D10.0%20Ki%3D0.1%20Kd%3D0.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D10.0%20Ki%3D1.0%20Kd%3D0.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D10.0%20Ki%3D10.0%20Kd%3D0.0].png)

It can be seen that the behavior was very similar.

Now if the Ki parameter is zeroed, you have a PD controller and you can observe the behavior of the derivative controller.

![Screenshot](FigSolver/degrau-Solvers[Kp%3D1.0%20Ki%3D0.0%20Kd%3D0.1].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D1.0%20Ki%3D0.0%20Kd%3D1.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D1.0%20Ki%3D0.0%20Kd%3D10.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D10.0%20Ki%3D0.0%20Kd%3D0.1].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D10.0%20Ki%3D0.0%20Kd%3D1.0].png)

![Screenshot](FigSolver/degrau-Solvers[Kp%3D10.0%20Ki%3D0.0%20Kd%3D10.0].png)